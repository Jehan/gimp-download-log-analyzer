#!/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Jehan
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this script.  If not, see <http://www.gnu.org/licenses/>.

import optparse
import gzip
import json
import sys
import os
import re

usage  = 'Usage: count-downloads.py access.log-YYYYMMDD.gz [ other.log ... ]\n'

cmdline = optparse.OptionParser(usage,
                                version="%prog, version 0.1",
                                description = "Count GIMP downloads from web logs",
                                conflict_handler="resolve")

(options, args) = cmdline.parse_args()

months = {
  'Jan' : 1,
  'Feb' : 2,
  'Mar' : 3,
  'Apr' : 4,
  'May' : 5,
  'Jun' : 6,
  'Jul' : 7,
  'Aug' : 8,
  'Sep' : 9,
  'Oct' : 10,
  'Nov' : 11,
  'Dec' : 12,
}

pattern=re.compile('^([0-9a-fA-F:.]*) .* \\[([0-9]*)/([a-zA-Z]*)/([0-9]*):[^]]*\\] "GET (?:/mirror)?/pub/gimp/v[0-9.]*/(windows|osx)/(gimp-([0-9a-zA-Z_.-]*).(dmg|exe)) .*$')

def process_log(logfile, logfile_path):
  outstats={}

  for line in logfile:
    match = pattern.match(line)
    if match is not None:
      ip = match.group(1)
      day = match.group(2)
      month = months[match.group(3)]
      year = match.group(4)

      outfile = 'download-stats-{}-{}.json'.format(year, month)
      if outfile not in outstats:
        outstats[outfile] = {
          'year': year,
          'month': month,
          'daily-stats': {}
        }
      stats=outstats[outfile]['daily-stats']

      platform = match.group(5)
      version = match.group(6)
      if day in stats:
        if platform in stats[day]:
          if version in stats[day][platform]:
            if ip not in stats[day][platform][version]:
              stats[day][platform][version] += [ip]
          else:
            stats[day][platform][version] = [ip]
        else:
          stats[day][platform] = { version : [ip] }
      else:
        stats[day] = {
          platform : { version : [ip] }
        }
  for outfile in outstats:
    stats=outstats[outfile]['daily-stats']
    for day in stats:
      for platform in stats[day]:
        for version in stats[day][platform]:
          # Drop all IP data and only keep number of unique IP. No
          # privacy-sensitive information is ever kept in these stats
          # once the script ends.
          stats[day][platform][version] = len(stats[day][platform][version])

    if os.path.exists(outfile):
      with open(outfile, 'r') as f:
        try:
          old_stats = json.load(f)
        except json.decoder.JSONDecodeError as error:
          sys.stderr.write('Error when decoding existing statistics "{}". Check the file or delete and regenerate it.\n'.format(outfile))
          continue
        if logfile_path in old_stats['processed-logs']:
          # We keep track of parsed log files.
          sys.stderr.write('Skipping {} for {}: already processed.\n'.format(logfile_path, outfile))
          continue
        outstats[outfile]['processed-logs'] = old_stats['processed-logs'] + [logfile_path]
        old_stats=old_stats['daily-stats']
        for day in old_stats:
          if day in stats:
            for platform in old_stats[day]:
              if platform in stats[day]:
                for version in old_stats[day][platform]:
                  if version in stats[day][platform]:
                    # A same day log can be spread across several files.
                    stats[day][platform][version] += old_stats[day][platform][version]
                  else:
                    stats[day][platform][version] = old_stats[day][platform][version]
              else:
                stats[day][platform] = old_stats[day][platform]
          else:
            stats[day] = old_stats[day]
    else:
      outstats[outfile]['processed-logs'] = [logfile_path]

    # Update monthly stats and add them to the stats.
    # TODO

    with open(outfile, 'w') as f:
      contents = json.dumps(outstats[outfile], indent=2)
      f.write(contents)

if __name__ == "__main__":
  if len(args) < 1:
    sys.stderr.write(usage)
    sys.exit(os.EX_USAGE)

  for arg in args:
    if arg[-3:] == '.gz':
      with gzip.open(arg, 'rt') as logfile:
        process_log(logfile, arg)
    else:
      with open(arg, 'rt') as logfile:
        process_log(logfile, arg)
